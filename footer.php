<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="footer">

		<div class="container">

			<div class="row">
				<div class="col-md-3">
					<h3>Mapping The Mind</h3>
					<div class="footer-links">
						<a href="/">Home</a>
						<a href="#">Buy Tickets</a>
						<a href="#">Speakers</a>
						<a href="#">Schedule</a>
						<a href="#">Contact</a>
					</div>
				</div>
				<div class="col-md-3">
					<h3>Social Media</h3>
					<div class="footer-links">
						<a href="https://www.facebook.com/mappingthemind/" target="_blank">Facebook</a>
						<a href="https://www.instagram.com/mappingthemind/" target="_blank">Instagram</a>
						<a href="https://www.youtube.com/channel/UCnfEe4JCjqX11Qa9PMFsOhw" target="_blank">YouTube</a>
					</div>
				</div>
		</div>

		</div>

	</div>
	<div class="copyright">

		<div class="container">
			<div class="row">
				<div class="col">
					<h4>Copyright 2019 Mapping The Mind</h4>
				</div>
			</div>
		</div>

	</div>

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>
