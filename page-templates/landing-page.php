<?php
/**
 * Template Name: landing page
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!--  -->

<div class="landing-page">
<div class="container">

<div class="row">
	<div class="col">
		<div class="page-introduction mtm-block">
			<h1>Mapping the Mind 2019</h1>
			<p>Psychedelic Science Conference</p>
			<p>September 21 2019 in Toronto, Canada</p>
			<a href="/event/mapping-the-mind-2019/" class="buy-btn">Buy Early Bird Tickets</a>
		</div>
	</div>
</div>

<div id="info" class="mtm-block">
	<div class="row">
		<div class="col">
			<div class="block-introduction">
				<h2>Some of this year's speakers include...</h2>
			</div>
		</div>		
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="profile">
				<img src="/wp-content/uploads/2019/02/denichol_jun15-directory.jpg" alt="">
				<h2>David E. Nichols, PhD</h2>
				<!-- <p>David is a distinguished Professor Emeritus at the Purdue University College of Pharmacy, and is recognized as one of the foremost international scientific experts on the medicinal chemistry of psychedelics.</p> -->
				<p>David is recognized as one of the foremost international scientific experts about psychedelics - his contributions include the synthesis and reporting of escaline, LSZ, 6-APB, 2C-I-NBOMe and other NBOMe variants as well as the coining of the term "entactogen".</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="profile">
				<img src="/wp-content/uploads/2019/02/600x600.jpg" alt="">
				<h2>Mendel Kaelen, PhD</h2>
				<p>Mendel is a post-doctoral neuroscientist at Imperial College London, specialised in the function of music in psychedelic therapy. Look forward to an interactive music workshop that simulates musical experiences in psychedelic therapy.</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="profile">
				<img src="/wp-content/uploads/2019/02/kenneth_tupper2.jpg" alt="">
				<h2>Dr. Kenneth Tupper</h2>
				<p>Since 1999, Dr. Tupper has studied the potential of psychedelic plants and substances to foster innovative thinking, to evoke experiences of wonder and awe, or to recover from addictions or eating disorders, when used carefully.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="block-feature">
				<h3>And many more speakers are yet to be announced. This year we will also be featuring Toronto Psychedelic Week activites leading up to the conference and hosting an afterparty lounge.</h3>
				<a href="/event/mapping-the-mind-2019/" class="buy-btn">Buy Early Bird Tickets</a>
			</div>
		</div>
	</div>
</div>

</div>
</div>

<!--  -->
<?php get_footer(); ?>
