<?php
/**
 * Template Name: about
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!--  -->
<div class="mushroom-img-container">
	<div class="container mmwm-container">
		<div class="row">
			<div class="col-md-12">
				<div class="content">
					<!--  -->
					<h1>About the Conference</h1>
					<h2>Profits made from this event are donated to research groups that fund research into psychedelic-assisted psychotherapy.</h2>
					<p> This year our fundraising focus is <a href="http://www.mapscanada.org" target="_blank" class="linkOut">MAPS Canada</a>.</p>
					<p>Psilocybin and related compounds have shown promise in treating a number of mental health ailments such as addiction, treatment-resistant depression, end of life anxiety and post-traumatic stress disorder.</p>
					<p>What is particularly interesting is that psychedelic medicines are only required several times in conjunction with psychotherapy compared to typical psychiatric medications, which are taken daily for indeterminate periods of time.</p>
					<p>This could indicate that psychedelic medicines may truly present a curative (cause-focused) approach to mental health care as opposed to a palliative (symptom-management focused) approach. Join us to learn, connect to the community, and make magic happen!</p>
					<h3>U of T CSSDP</h3>
					<p>This conference is hosted by the <a href="https://www.facebook.com/CSSDPUofT/" target="_blank" class="linkOut">University of Toronto chapter of the Canadian Students for Sensible Drug Policy</a>; a grassroots nonprofit dedicated to drug policy reform based on the principles of harm reduction.</p>
					<h3>920 Coalition</h3>
					<p>This event is in affiliation with the <a href="http://www.920coalition.org" target="_blank" class="linkOut">920coalition</a>; a network of indiviuals and groups that host events to raise awareness of the benefits and uses for ‘magic’ mushrooms every September 20th.</p>
					<h3>MAPS</h3>
					<p>Founded in 1986, the <a href="maps.org" class="linkOut">Multidisciplinary Association for Psychedelic Studies (MAPS)</a> is a non-profit research and educational organization that develops medical, legal, and cultural contexts for people to benefit from the careful uses of psychedelics and marijuana.</p>
					<!--  -->
				</div>
			</div>
		</div>
	</div>	
</div>
<!--  -->
<?php get_footer(); ?>
