<?php
/**
 * Template Name: schedule
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!--  -->
<div class="mushroom-img-container">

	<div class="container mmwm-container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<!--  -->
				<div class="entry-content">
					<h1>Schedule</h1>
					<p>You can read the full detail about presentations and talk outlines on our <a href="/speakers/">speakers page</a>.</p>

					<table class="table">
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">Timeslot</th>
					      <th scope="col">Session</th>
					      <th scope="col">Location</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <th scope="row">8:00am-9:00am</th>
					      <td>Morning Yoga</td>
					      <td>Queen's Park</td>
					    </tr>
					    <tr>
					      <th scope="row">8:45am</th>
					      <td>Conference sign in begins</td>
					      <td>Earth Sciences Center</td>
					    </tr>
					    <tr>
					      <th scope="row">9:20am – 9:30am</th>
					      <td>Opening Remarks</td>
					      <td>ES-1050</td>
					    </tr>
							<tr>
								<th scope="row">9:30am – 10:15am</th>
								<td><span class="speaker-table">Mark Haden</span></td>
								<td>ES-1050</td>
							</tr>
							<!--  -->
							<tr>
								<th scope="row">10:30am – 11:15am</th>
								<td><span class="speaker-table">John Vervaeke</span></td>
								<td>ES-1050</td>
							</tr>
							<tr>
								<th scope="row"></th>
								<td><span class="speaker-table">Rotem Petranker</span></td>
								<td>B-142</td>
							</tr>
							<tr>
								<th scope="row"></th>
								<td>Reagent Drug Checking Demo</td>
								<td>B-149</td>
							</tr>
							<!--  -->
							<!--  -->
							<tr>
								<th scope="row">11:30am – 12:15pm</th>
								<td><span class="speaker-table">Sophia Korb</span></td>
								<td>ES-1050</td>
							</tr>
							<tr>
								<th scope="row"></th>
								<td><span class="speaker-table">Dan Greig</span></td>
								<td>B-142</td>
							</tr>
							<tr>
								<th scope="row"></th>
								<td>Mushroom Growing Workshop</td>
								<td>B-149</td>
							</tr>
							<!--  -->
							<tr>
								<th scope="row">12:15pm – 1:30pm</th>
								<td>Lunch (Not provided)</td>
								<td></td>
							</tr>
							<!--  -->
							<tr>
								<th scope="row">12:45pm – 1:30pm</th>
								<td>Lunch and Learn | <span class="speaker-table">Ron Shore</span></td>
								<td>B-142</td>
							</tr>
							<!--  -->
							<tr>
								<th scope="row">1:40pm – 2:25pm</th>
								<td>Anne Wagner</td>
								<td>ES-1050</td>
							</tr>
							<tr>
								<th scope="row"></th>
								<td><span class="speaker-table">Heather Hargraves</td>
								<td>B-142</td>
							</tr>
							<tr>
								<th scope="row"></th>
								<td>Mushroom Growing Workshop</td>
								<td>B-149</td>
							</tr>
							<!--  -->
							<tr>
								<th scope="row">2:35pm – 3:35pm</th>
								<td><span class="speaker-table">Samantha Podrebarac</td>
								<td>ES-1050</td>
							</tr>
							<!--  -->
							<tr>
								<th scope="row">3:45pm – 4:45pm</th>
								<td><span class="speaker-table">Trevor Millar</td>
								<td>ES-1050</td>
							</tr>
							<!--  -->
							<tr>
								<th scope="row">4:45pm – 5:12pm</th>
								<td>Break</td>
								<td></td>
							</tr>
							<!--  -->
							<tr>
								<th scope="row">5:15pm – 6:30pm</th>
								<td>Panel discussion: “The Future of Psychedelic Science”</td>
								<td>ES-1050</td>
							</tr>
							<!--  -->
					  </tbody>
					</table>

				</div>
				<!--  -->
			</div>
		</div>
	</div>
</div>

</div>
<!--  -->
<?php get_footer(); ?>
