<?php
/**
 * Template Name: content-page
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!--  -->

<div class="container mmwm-container">
	<div class="row">
		<div class="col-md-12">

		</div>
	</div>
</div>

<!--  -->
<?php get_footer(); ?>
