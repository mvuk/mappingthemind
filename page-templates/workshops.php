<?php
/**
 * Template Name: workshops
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!--  -->
<div class="mushroom-img-container">
	<div class="container mmwm-container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<h1>Workshops</h1>
				<p>Workshops have limited seating and given on a first come first serve basis. Please come early if you would like to reserve your place!</p>
			</div>
		</div>
	</div>
	<!-- <div class="row">
		<div class="col-md-12">
			<div class="content">
				<h2>Morning Yoga</h2>
				<p>Hosted by Lu of <a href="https://www.ganjayogatoronto.com/the-studio">House of Yoga</a>, op by early for a guided Yoga session to get you ready for the conference!</p>
				<img src="/wp-content/uploads/2018/08/houseofyoga.png" alt="">
			</div>
		</div>
	</div> -->
	<!--  -->
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<h2>Mushroom Growing Workshop</h2>
				<p>Use the PF Tek method to plant and grow your own mushroom jar! This workshop will be hosted by CSSDP member and Mushroom Geurilla founder Anthony Cillero. Capacity is 100 spread across two workshops. $2 donation suggested for supplies!</p>
				<p>Two workshops at a capacity of 50 each</p>
				<img src="/wp-content/uploads/2018/08/mushroomguerrillalogo-300x300.jpg" alt="">
			</div>
		</div>
	</div>
	<!--  -->
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<h2>Reagent Drug Checking Workshop</h2>
				<p>Hosted by the creator of <a href="http://www.checkitkit.ca">checkitkit.ca</a>, learn how to use reagent test kits! These chemical additives can be applied to recreational substances to test for the presence of adulterants.</p>
				<p>Capacity = 100</p>
				<img src="/wp-content/uploads/2018/08/checkit1-1-768x377.jpg" alt="">
			</div>
		</div>
	</div>
	<!--  -->
</div>
</div>
<!--  -->
<?php get_footer(); ?>
