<?php
/**
 * Template Name: dinner-ap
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!--  -->

<div class="mushroom-img-container">
	<div class="container mmwm-container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<h1>Dinner with Speakers</h1>
				<p>This is your chance to attend a private dinner event with speakers from the conference. Enjoy a vegan restaurant with discussions of scientific psychedelic topics.</p>
				<p>Planta is a innovative plant-based restaurant inspired by all parts of the world located in Toronto, Canada and South Beach, USA.</p>
				<p><strong>Planta Toronto</strong><br>1221 Bay St<br>Toronto, ON M5R 3P5</p>
				<a href="/event/mapping-the-mind-with-mushrooms/" class="tickets">Buy Tickets</a>
				<div class="row">
					<div class="col-md-6">
						<img src="/wp-content/uploads/2018/08/toronto-restaurants-planta-vegetarian-the-chase-david-lee-yorkville-room-1.jpg" alt="">
					</div>
					<div class="col-md-6">
						<img src="/wp-content/uploads/2018/08/PlantaTorontoInterior02-1080x700.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<h1>After Party</h1>
				<p>“If I Can’t Dance, I Don’t Want To Be Part of Your Revolution” – Emma Goldman</p>
				<p>What better way to facilitate the Psychedelic Research Renaissance than with dance and community? We will be hosting an official after party to raise further funds to be donated to MAPS Canada.</p>
				<div class="row">
					<div class="col-md-12">
						<h2>Lineup</h2>
					</div>
					<div class="col-md-12">
						<h3>Main Room</h3>
					</div>
					<div class="col-md-6">
						<img src="/wp-content/uploads/2018/08/equus.jpg" alt="">
						<h4>Equus</h4>
						<p><strong>[Techno]</strong></p>
						<p>Equus strives to take dancers on a ride; a journey through the art of techno. With an appreciation for ambient, classical, jazz and hip-hop, Equus isn't a one trick pony when it comes to their craft. Saddle up, expect the unexpected, and remember...no hoof, no horse.</p>
						<a href="https://soundcloud.com/no_horseplay" class="artist-link" target="_blank">SoundCloud</a>
						<a href="https://www.facebook.com/no.horseplay" class="artist-link" target="_blank">FaceBook</a>
						<a href="https://www.residentadvisor.net/dj/equus" class="artist-link" target="_blank">Resident Advisor</a>
					</div>
					<div class="col-md-6">
						<img src="/wp-content/uploads/2018/08/jonah-k.jpg" alt="">
						<h4>Jonah K</h4>
						<p><strong>[Bass]</strong></p>
						<p>Jonah K is an A/V alchemist specializing in atmospheric, bass-driven music, detailed illustration and dynamic animation. Taking inspiration from mysterious places, strange creatures and the outskirts of the sub-conscious, Jonah is a fearless explorer of the imagination and his work conjures deep and vivid narratives.​</p>
						<a href="https://soundcloud.com/teknotribes" class="artist-link" target="_blank">SoundCloud</a>
						<a href="https://www.facebook.com/the.Jonah.K" class="artist-link" target="_blank">FaceBook</a>
						<a href="http://www.jonah-k.com/" class="artist-link" target="_blank">Website</a>

					</div>
					<div class="col-md-6">
						<img src="/wp-content/uploads/2018/08/ankle-mix.jpg" alt="">
						<h4>Ankle MiX</h4>
						<p><strong>[Electronic]</strong></p>
						<p>Born four thousand four hundred and forty four years in the future, deep inside the caves of Yemen along the coast of the Red Sea, raised by a nomadic flock of technicolour Pelicans, Ankle MiX is a child in time who transcends bounds of tradition & convention to deliver unexpected sounds in unexpected ways for a delicately balanced taste.</p>
						<a href="https://soundcloud.com/anklemix" class="artist-link" target="_blank">SoundCloud</a>
						<a href="https://www.facebook.com/pg/smurfminded/" class="artist-link" target="_blank">FaceBook</a>
						<a href="https://www.mixcloud.com/amixofloveandlove/" class="artist-link" target="_blank">MixCloud</a>
					</div>
					<div class="col-md-12">
						<h3>Ambient Lounge</h3>
					</div>
					<div class="col-md-12">
						<img src="/wp-content/uploads/2018/08/Psychedelic-storytelling.jpg" alt="">
						<h4>Psychedelic StoryTelling</h4>
						<p>Have a powerful, insightful, comical or spiritual experience to share? This is your opportunity to connect with others</p>
					</div>
				</div>

				<h3>Location</h3>
				<p><strong>Tranzac Club</strong><br>292 Brunswick Ave<br>Toronto, ON M5S 2M7</p>
				<p><strong>Advance Tickets:</strong> $15</p>
				<p><strong>Door Price:</strong> $20</p>
				<p><strong>Time:</strong> 9pm – 3am.</p>
				<a href="/event/mapping-the-mind-with-mushrooms/" class="tickets">Buy Tickets</a>
				<img src="/wp-content/uploads/2018/08/Tranzac-Map-1.jpg" alt="">
			</div>
		</div>
	</div>
	<!--  -->
</div>
</div>

<!--  -->
<?php get_footer(); ?>
