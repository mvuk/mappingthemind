<?php
/**
 * Template Name: home page
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!--  -->

<div class="mmwm-wrap">

	<div class="container-fluid fullHeightContainer">
		<div class="row fullHeightRow">
			<div class="col-md-6 poster-col">
				<img src="/wp-content/uploads/2018/08/mmwm-2018-poster-edit.jpg" alt="" class="poster">
			</div>
			<div class="col-md-6">
				<div class="content-wrap">
					<div class="content">
							<h1>Mapping the Mind with Mushrooms 2018</h1>
							<h2>An annual conference that brings together psychologists, philosophers and mycologists to address the current research and findings about psychedelic mushrooms.</h2>
							<a href="/event/mapping-the-mind-with-mushrooms/" class="tickets">Buy Tickets</a>
							<p>September 22, 2018</p>
							<p>5 Bancroft Ave<br>Earth Sciences Center<br>University of Toronto<br>Toronto, Canada</p>
							<img src="/wp-content/uploads/2018/08/Logos.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<!--  -->
<?php get_footer(); ?>
