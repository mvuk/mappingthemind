<?php
/**
 * Template Name: speakers
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!--  -->

<div class="mushroom-img-container">
	<div class="container mmwm-container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<h1>Speakers</h1>
				<p>Scientists and professionals in the psychedelic sphere come together to educate you. Explore the biographies of our speakers and the outlines for their presentations.</p>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="content section">
		<div class="row">
			<div class="col-md-6">
				<img src="/wp-content/uploads/2018/08/mark-haden.jpg" alt="">
			</div>
			<div class="col-md-12">
					<h2>Mark Haden</h2>
					<h3>Biography</h3>
					<p>Mark Haden is an adjunct professor at the University of British Columbia School of Public and Population Health. He is the  Executive Director of MAPS Canada (Multidisciplinary Association for Psychedelic Studies) and has published on the issue of drug control policy (including psychedelics) in the journals such as the Canadian Journal of Public Health and the Journal of Psychoactive Drugs. Mark has worke for the addiction services for 28 years in counselling, and supervisory roles. He also offers private practice counselling to individuals dealing with a wide variety of substance use issues from parents who are dealing with alcohol, drug and special needs issues to psychedelic experience integration.</p>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="content section">

		<div class="row">
			<div class="col-md-6">
				<img src="/wp-content/uploads/2018/08/Samantha_Podrebarac-name.jpg" alt="">
			</div>
			<div class="col-md-12">
				<h2>Samantha Podrebarac</h2>
				<h3>Biography</h3>
				<p>Samantha Podrebarac, M.Sc., is a researcher with the NYU experimental therapeutics team. She is involved in the phase-II randomized double-blind trial of psilocybin-assisted treatment of alcohol use disorder and a neuroimaging study on the same patient population taking place at NYU. Prior to joining NYU, Samantha completed her MSc in neuroscience through the Brain and Mind Institute at the University of Western Ontario. She is completing a graduate degree in Spirituality and Mind-Body Psychology through Columbia University. Samantha’s interests are focused on the use of expanded states of consciousness in medicine (through both psychedelic and non-psychedelic routes) to facilitate access to the innate healing capacity that exists within each individual. She is also interested in the use of adjunct practices to sustain and support the perceptual opening that patients treated with psychedelic medicine in clinical trials experience, in order to further their own access to spiritual and psychological insight and guidance along their journey.</p>
				<h3>Outline</h3>
				<p>This talk will feature an overview of the participants recruited and treated to-date in the phase-II randomized double-blind trial of psilocybin assisted treatment of alcohol use disorder trial at NYU. Preliminary results will be presented comparing the intensity of the medication experience to the drinking outcomes of participants in the trial. The unique journey of a subset of participants in the study, including medication session experiences, will be presented. Further discussions of the possible mechanisms, clinical observations, and importantly the broader implications of this work, will be explored.</p>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="content section">
		<div class="row">
			<div class="col-md-6">
				<img src="/wp-content/uploads/2018/08/Trev-Head-2.jpg" alt="">
			</div>
			<div class="col-md-12">
					<h2>Trevor Millar</h2>
					<h3>Biography</h3>
					<p>Trevor Millar is a social-entrepreneur and owner of Liberty Root Therapy Ltd., serving those called to experience the healing properties of African plant medicine Tabernanthe Iboga and its derivatives.  Iboga is a powerful psychedelic, an exceptional addiction interrupter, and recent discoveries show it's also got potential for treating Parkinson's Disease and other neurological disorders.  Trevor currently serves as Board Chair for MAPS Canada and is a former Executive Director of the Global Ibogaine Therapy Alliance.</p>
					<h3>Outline</h3>
					<p>Trevor's presentation will touch upon the traditional use of Iboga within the African context, introduce how it’s anti-addictive properties were uncovered in the West and how that ties to the formation of Liberty Root Therapy Ltd., which has now ushered more than 200 people through the iboga experience.  As the cutting-edge of psychedelic therapy quickly becomes more mainstream, Trevor will look at various avenues for the further legitimization of these medicines and which contexts may best serve this work and the psychedelic community overall as we move forward.</p>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="content section">
		<div class="row">
			<div class="col-md-6">
				<img src="/wp-content/uploads/2018/08/heather-hargraves.jpg" alt="">
			</div>
			<div class="col-md-12">
					<h2>Heather Hargraves, MA</h2>
					<h3>Biography</h3>
					<p>Trauma therapist and researcher from London, Ontario, Canada. Heather specializes in the use of neuro and biofeedback technologies to support a broad range of neurologically and/or psychologically traumatized clients within her clinical practice. Heather’s therapeutic use of brain computer technologies offers a method for documenting and training self-directed awareness to support self-regulated and resilient “states” within the brain/ body connection. Self-directed awareness, as guided by brain computer technology, then works to slowly normalize and redirect aberrant (but in part “compensatory”) neural processes within each individual client. Heather’s research interests investigate the neurological underpinnings of various states of consciousness, including dissociation, meditation, psychedelics and various polyphasic states of consciousness, associated with shamanic practices. </p>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="content section">
		<div class="row">
			<div class="col-md-6">
				<img src="/wp-content/uploads/2018/08/john-vervaeke.jpg" alt="">
			</div>
			<div class="col-md-12">
					<h2>John Vervaeke</h2>
					<h3>Biography</h3>
					<p>John Vervaeke is an Assistant Professor, in the teaching stream. He has been teaching at the University of Toronto since 1994. He currently teaches courses in the Psychology department on thinking and reasoning with an emphasis on insight problem solving, cognitive development with an emphasis on the dynamical nature of development, and higher cognitive processes with an emphasis on intelligence, rationality, mindfulness, and the Psychology of wisdom. He also teaches courses in the Cognitive Science program on the introduction to Cognitive Science, and the Cognitive Science of consciousness. In addition, he teaches a course in the Buddhism, Psychology and Mental Health program on Buddhism and Cognitive Science. He is the director of the Consciousness and the Wisdom Studies Laboratory. He has won and been nominated for several teaching awards including the 2001 Students Administrative Council and Association of Part-time Undergraduate Students Teaching Award for the Humanities, and the 2012 Ranjini Ghosh Excellence in Teaching Award. He has published articles on relevance realization, general intelligence, mindfulness, flow, metaphor, and wisdom. He is first author of the book Zombies in Western Culture: A 21 st Century crisis which integrates Psychology and Cognitive Science to address the meaning crisis in Western society.</p>
					<h3>Outline</h3>
					<p>What is higher about higher states of consciousness? Very often and prototypically when people return from an altered state of consciousness (ASC), for example dreaming, they will state that the experiences within that alternative state were an illusion, and that they have returned to reality by returning to everyday consciousness and cognition. However, there is a subset of ASCs that have the opposite effect. On returning to everyday consciousness and cognition people pronounce their altered state was more real or really real, and their everyday state is found to be deficient in realness and/or being. These states are often called higher states of consciousness (HSC) and are described cross historically and cross culturally by metaphors of enlightenment and awakening. These states and their claim to superiority have figured as crucial to many of the axial religions, and their claim to superiority is plausibly the basis for their ability to bring about significant transformation, whether therapeutic or existential, in individuals. Let’s call the purported superiority of HSCs onto-normativity, and then ask two distinct but related questions. The first question is what is the cognitive bases of the claim and its attendant phenomenological qualities? The second question is do these states actually provide a rational justification for onto-normativity. To provide an answer to the first question is to provide a psychological theory of onto-normativity. The answer to the second question provides an epistemological theory of onto-normativity. The talk will present an integrated psychological and epistemological theory of the onto-normativity of HSCs.</p>
			</div>
		</div>
	</div>

	<!--  -->
	<div class="content section">
		<div class="row">
			<div class="col-md-6">
				<img src="/wp-content/uploads/2018/08/Sophia-korb.jpg" alt="">
			</div>
			<div class="col-md-12">
					<h2>Sophia Korb</h2>
					<h3>Biography</h3>
					<p>Sophia Korb, Ph.D., holds a masters and Ph.D. in clinical psychology from the Institute of Transpersonal Psychology. Dr. Korb’s clinical work thus far focuses on people with severe and persistent mental illnesses. Dr. Korb consults on research design and statistical methods on diverse projects from New York City neighborhood mental health resource utilization to bee sonification. She is passionate about participants' engagement and ownership of research.</p>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="content section">
		<div class="row">
			<!-- <div class="col-md-5">
				<img src="/wp-content/uploads/2018/08/Sophia-korb.jpg" alt="">
			</div> -->
			<div class="col-md-12">
					<h2>Ron Shore</h2>
					<h3>Biography</h3>
					<p>Ron Shore started working in the harm reduction community as a twenty-one year old at the height of the AIDS epidemic. He was the first federally-funded AIDS counsellor and outreach worker for prisoners in Canada. He and his colleagues went on to found the Keep Six! Needle Exchange and the Street Health Centre, a multi-service harm reduction health centre in Kingston. Ron is now entering the second year of a PhD focused on psychedelic medicine at Queen’s University. He has taught Introduction to the Study of Alcohol and Drug Problems at Queen’s for 12 years. He has a particular interest in the study of shamanism, ayahausca and the wondrous mushroom. </p>
					<h3>Outline</h3>
					<p>Best Practices in Psilocybin: What do we know?</p>
					<p>Magic mushrooms face an interesting journey as they enter the mainstream of popular consciousness and treatment therapies. This will force a reconsideration of our current allopathic, deficit-based treatment system of medical-pharmaceutical practice and poses challenges to contemporary drug, social, and health policy. This presentation draws from the history of harm reduction in Canada, highlights the emergent psilocybin-related scientific breakthroughs and presents an initial evidence-based model of best practices for the expansion of pscilocybin and psilocybin based therapies in Canada. A model of healing-centred engagement based on community, connection and techniques of personal tuning is presented. The model draws from historical traditions of magic mushroom use and attempts to translate shamanistic methods into the contemporary, Western landscape.</p>
			</div>
		</div>
	</div>


	<!--  -->
	<div class="content section">
		<div class="row">
			<div class="col-md-6">
				<img src="/wp-content/uploads/2018/08/Rotem-Petranker.jpg" alt="">
			</div>
			<div class="col-md-12">
					<h2>Rotem Petranker</h2>
					<h3>Biography</h3>
					<p>Rotem has a Bsc in psychology from the University of Toronto and a MA in social psychology from York University. He is currently a PhD student in York's clinical psychology program, in John Eastwood's lab. His main research interest is affect regulation, and the way it interacts with sustained attention, mind wandering, and creativity. He is a founding member oft the Toronto Psychedelic Interest Group (PSIG) along with Thomas Anderson, Cory Weissman, and Daniel Rosenbaum. Rotem has presented original research findings on psychedelic research in a few conferences, currently has two coauthored papers detailing these findings under review, and is in the process of putting together a RCT study of microdosing.</p>
					<h3>Outline</h3>
					<p>This talk will present key findings from two microdosing papers currently under review, examining different aspects of the data obtained from some 1390 participants who responded to an online survey. The first part of the talk will highlight demographic information and a taxonomy of reported benefits and drawbacks of microdosing. The second part of the talk will focus on our pre-registered hypotheses and the differences we found between microdosers and non-microdosers, with a particular focus on well-being and creativity.</p>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="content section">

		<div class="row">
			<div class="col-md-6">
				<img src="/wp-content/uploads/2018/08/Anne-Wagner-300x300-name.jpg" alt="">
			</div>
			<div class="col-md-12">
				<h2>Anne Wagner</h2>
				<h3>Biography</h3>
				<p>Dr. Anne Wagner is a clinical psychologist and researcher who is committed to helping understand and improve trauma recovery. She is the founder of Remedy, a mental health innovation community, and an associate member of the Yeates School of Graduate Studies at Ryerson University. She completed a Canadian Institutes of Health Research Postdoctoral Fellowship at Ryerson University. She is the Chair of the Traumatic Stress Section of the Canadian Psychological Association, and sits on the Quality Committee of Casey House (Toronto’s HIV/AIDS Hospital). Anne has a particular focus on innovating mental health interventions, for example by working with a community-led approach (e.g., with HIV/AIDS service organizations and community health centres), using different treatment formats (e.g., with couples), and facilitators of treatment (e.g., MDMA). Anne, alongside Dr. Michael Mithoefer, Annie Mithoefer, BSN, and Dr. Candice Monson, is one of the investigators of the MAPS funded pilot study of Cognitive Behavioral Conjoint Therapy for PTSD + MDMA. Anne is the lead investigator for the upcoming MAPS funded pilot study of Cognitive Processing Therapy for PTSD + MDMA.</p>
				<h3>Outline</h3>
				<p>Updates from the Field: MDMA+Cognitive Behavioral Conjoint Therapy for PTSD and MDMA+Cognitive Processing Therapy for PTSD</p>
				<p>The pilot study of MDMA+Cognitive Behavioral Conjoint Therapy (CBCT) for PTSD completed in the spring of 2018, with 6 couples going through the protocol. What did those couples look like at the end of treatment and through follow-up? Now that the pilot is complete, what comes next? This presentation will give an overview of how this pilot study, as well as the next, MDMA+Cognitive Processing Therapy (CPT) for PTSD, fit into the landscape of the use of MDMA in the treatment of PTSD, and will discuss how they can serve to answer questions, and create new ones, regarding MDMA’s potential as an adjunct to psychotherapy.</p>
			</div>
		</div>
	</div>
	<!--  -->
	<div class="content section">
		<div class="row">
			<div class="col-md-6">
				<img src="/wp-content/uploads/2018/08/Daniel-Greig.jpg" alt="">
			</div>
			<div class="col-md-12">
				<h2>Daniel Greig</h2>
				<h3>Biography</h3>
				<p>Daniel is a student of Cognitive Science and Philosophy at the University of Toronto. His focus is on wisdom, magic, trauma, and the psychedelic experience through the lens of psychology and neuroscience. Daniel is an active member of the Canadian Students for Sensible Drug Policy (CSSDP). He has also spoken at conferences and educational events internationally on the subject of psychedelics and philosophy.</p>
			</div>
		</div>
	</div>

	<!--  -->

</div>
</div>

<!--  -->
<?php get_footer(); ?>
